from rest_framework import routers
from orders import views

app_name = 'orders'

router = routers.SimpleRouter(trailing_slash=False)
router.register(r'orders', views.OrderViewSet, base_name='orders')

urlpatterns = router.urls
