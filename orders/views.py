from orders.models import Order
from rest_framework import viewsets

from orders.serializers import OrderSerializer


class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer

    def perform_destroy(self, instance):
        instance.deleted = True
        instance.save()

    # def perform_update(self, serializer):
    #     pass

