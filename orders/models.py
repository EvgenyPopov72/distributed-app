from django.db import models


class Order(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=100, unique=False, blank=False, null=False)
    deleted = models.BooleanField(default=False)

    class Meta:
        ordering = ('created',)
